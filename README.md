This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Added features:
* global reset CSS,
* Aliases
* initial folder structure
* ES lint
* prettier
* cherry picked lodash
* scss
