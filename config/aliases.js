const path = require("path");

const aliases = {
    components: path.resolve(__dirname, "../src/components"),
    ui: path.resolve(__dirname, "../src/components/__ui"),
    store: path.resolve(__dirname, "../src/store"),
    shared: path.resolve(__dirname, "../src/shared"),
    assets: path.resolve(__dirname, "../src/assets")
};

module.exports = aliases;