import { store } from "components/App";

export const createDispatch = () => store.dispatch;