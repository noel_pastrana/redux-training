export const __IS_PROD___ = process.env.NODE_ENV === "production";
export const __IS_DEV___ = !__IS_PROD___;
