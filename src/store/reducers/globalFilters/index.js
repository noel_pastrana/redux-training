import GLOBAL_FILTER_ACTIONS from "store/actionCreators/globalFilters/actions";

const initialState = {
  isMenuOpen: false,
};

export const globalFiltersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GLOBAL_FILTER_ACTIONS.GLOBAL_FILTERS_MENU_SHOW:
      return {
        ...state,
        isMenuOpen: true,
      };

    case GLOBAL_FILTER_ACTIONS.GLOBAL_FILTERS_MENU_HIDE:
      return {
        ...state,
        isMenuOpen: false,
      };

    default:
      return state;
  }
};
