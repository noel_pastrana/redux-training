import {
  GLOBAL_FILTERS_ACTIONS,
  GlobalFiltersActionsTypes
} from "store/actionCreators/globalFilters/actions";

const initialState = {};

export const globalFiltersReducer = (
  state: object = initialState,
  action: GlobalFiltersActionsTypes
): object => {
  switch (action.type) {
    case GLOBAL_FILTERS_ACTIONS.GLOBAL_FILTERS_CHANGE: {
      return { ...state, ...action.payload };
    }
    default:
      return state;
  }
};
