import { combineReducers } from "redux";
import { usersReducer, userReducer } from "./usersReducer";


export const rootReducer = combineReducers({
  users: usersReducer
});