import FETCH_DATA_ACTIONS from "store/actionCreators/user/actions";

const initialState = {
  users: [],
  userId: ''
};

export const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    
    case FETCH_DATA_ACTIONS.FETCH_USERS_SUCCESS:
      return {
        ...state,
        users: action.data
      };
      
    case FETCH_DATA_ACTIONS.FETCH_USER_BY_ID:

      const newState =  {
        ...state,
        userId: action.payload
      };

      return newState
    
    default:
      return state;
  }
};

