import { combineReducers } from "redux";
import { globalFiltersReducer } from "./globalFilters";

export const rootReducer = combineReducers({
  globalFilters: globalFiltersReducer
});
