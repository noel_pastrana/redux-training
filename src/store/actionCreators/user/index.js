import FETCH_DATA_ACTIONS from "./actions";
import axios from "axios";

// action creators - function
export const fetchUsersSuccess = (data) => ({
  type: FETCH_DATA_ACTIONS.FETCH_USERS_SUCCESS,
  data,
});

// action creators - function
export const fetchUserByIdSuccess = (id) => ({
  type: FETCH_DATA_ACTIONS.FETCH_USER_BY_ID,
  payload: id
});



export const fetchUsers = () => {
  const promise = axios.get("https://jsonplaceholder.typicode.com/users");

  return (dispatch, getState) => {
    promise.then(response => {
      dispatch(fetchUsersSuccess(response.data))
    });
  }
};


export const selectUserId = (key) => {
  return (dispatch, getState) => {
   return dispatch(fetchUserByIdSuccess(key));
  }
};
  