import { GLOBAL_FILTERS_ACTIONS, GlobalFiltersChangeType } from "./actions";

// todo wonder if get rid of class and just do fns
export class GlobalFiltersActionCreators {
  static change = (nextGlobalFilters: object): GlobalFiltersChangeType => ({
    type: GLOBAL_FILTERS_ACTIONS.GLOBAL_FILTERS_CHANGE,
    payload: nextGlobalFilters
  });
}
