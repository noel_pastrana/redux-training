import { ActionType } from "shared/types/action";

export enum GLOBAL_FILTERS_ACTIONS {
  GLOBAL_FILTERS_CHANGE = "GLOBAL_FILTERS_CHANGE"
}

export type GlobalFiltersChangeType = ActionType<
  GLOBAL_FILTERS_ACTIONS.GLOBAL_FILTERS_CHANGE,
  object
>;

export type GlobalFiltersActionsTypes = GlobalFiltersChangeType;
