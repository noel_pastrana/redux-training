import GLOBAL_FILTER_ACTIONS from "./actions";

// action creators
export const showMenuGlobalFilters = () => ({
  type: GLOBAL_FILTER_ACTIONS.GLOBAL_FILTERS_MENU_SHOW,
});

export const hideMenuGlobalFilters = () => ({
  type: GLOBAL_FILTER_ACTIONS.GLOBAL_FILTERS_MENU_HIDE,
});