import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import { rootReducer } from "./reducers/rootReducer";

export const configuredStore = (() => {
  const middlewares = [reduxThunk];
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middlewares))
  );

  return store;
})();
