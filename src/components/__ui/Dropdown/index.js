import React, { Component } from "react";
import "./index.scss";

class Dropdown extends Component {
  state = {
    active: "",
    isOpen: false
  };

  componentDidMount() {
    console.log("Console log from component did mount");
    document.addEventListener("click", this.handleClickOutside);
  }

  componentWillUnmount() {
    console.log("Console log from component WILL UNMOUNT");
  }

  dropdown = React.createRef();

  openHandler = () => {
    this.setState(state => ({ isOpen: !state.isOpen }));
  };

  selectHandler = active => {
    this.setState(state => ({ active: active }));
  };

  handleClickOutside = e => {
    console.log(this.dropdown.current);
    console.log(e.target);

    if (this.state.isOpen && !this.dropdown.current.contains(e.target)) {
      this.setState({ isOpen: false });
    }
  };

  render() {
    const { options } = this.props;
    const { isOpen, active } = this.state;
    const selectedOption = active ? active : options[0];

    return (
      <div className="dropdown" ref={this.dropdown} onClick={this.openHandler}>
        <div>{selectedOption}</div>
        {isOpen && (
          <div className="dropdown__wrapper">
            {options.map(option => (
              <div
                key={option}
                className="dropdown__option"
                onClick={() => this.selectHandler(option)}
              >
                {option}
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

export default Dropdown;
