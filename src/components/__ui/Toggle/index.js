import React from "react";
import "./index.scss";
import cx from "classnames";



const Toggle = ({ active, onClickHandler }) => {

  return (
    <div className="toggle" onClick={onClickHandler}>
      <div
        className={cx("toggle__circle", {
          "toggle__circle--active": active
        })}
      />
    </div>
  );
};

export default Toggle;
