import React from "react";
import { connect } from "react-redux";
import { hideMenuGlobalFilters } from "store/actionCreators/globalFilters";
import "./index.scss";

const Menu = (props) => {
  const onClickClose = () => {
  };

  return (
    <div className="menu">
      <div className="menu__close" onClick={onClickClose}>&times;</div>
    </div>
  );
};


const mapStateToProps = state => ({
  isMenuOpen: state.globalFilters.isMenuOpen
});


export default connect(mapStateToProps)(Menu);
