import React from "react";
import "./index.scss";
import SwitcherOption from "components/__ui/Switcher/SwitcherOption";

const Switcher = ({ options, active, onOptionClick }) => {
  const optionsLength = options.length;
  const activeOptionIndex = options.findIndex(option => option === active);

  const onOptionClickHandler = (active) => {
    onOptionClick(active);
  };

  return (
    <div className="switcher">
      {options.map((option ) => {
        return (
          <SwitcherOption
            key={option}
            value={option}
            onOptionClick={() => onOptionClickHandler(option)}
          />
        )
      })}
      <span style={{ width: `${100 / optionsLength}%`, transform: `translateX(${activeOptionIndex * 100}%)`}}
            className="switcher__active-bar"
      />
    </div>
  );
};

export default Switcher;
