import React from "react";

const SwitcherOption = ({ value, onOptionClick }) => {
  return <div className="switcher__option" onClick={onOptionClick}>{value}</div>;
};

export default SwitcherOption;
