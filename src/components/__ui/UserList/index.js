import React, { Fragment } from 'react';
import './index.scss';



const UserList = (props) => {
    const {users, onClickHandleUser} = props;

    return (
        <Fragment>

            <div className="row">
                <br/>
                <div className="btn-group-vertical d-block" style={{minWidth: 300}} role="group" aria-label="Basic example">
                    {users.map((user, id) => {
                        return <button onClick={()=>{onClickHandleUser({id})}} type="button btn-block" className="btn btn-secondary" key={id}>{user.name}</button>
                    })}
                </div>
            </div>
            
        </Fragment>    
    )
}


export default UserList;