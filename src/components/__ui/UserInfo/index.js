import React, { Fragment } from 'react';
import './index.scss';

//Dependencies

const UserInfo = (props) => {
    
    return (
        <Fragment>
            <div className="row">
                <br/>
                <div className="userInfo">{props.user}</div>
            </div>
        </Fragment>    
    )
}

export default UserInfo;