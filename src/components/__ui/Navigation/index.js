import React from 'react';
import "./index.scss";

// Dependencies
import {Link} from "react-router-dom";

const Navigtion = () => {
    return (
        <div className="navigation">
            <ul className="navigation__list">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/product">Product</Link></li>
                <li><Link to="/join-us">Join Us</Link></li>
                <li><Link to="/contact-us">Contact Us</Link></li>
            </ul>
        </div>
    )
}

export default Navigtion;