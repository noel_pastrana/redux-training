import React from "react";
import "./index.scss";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Provider } from "react-redux";
import { HashRouter } from "react-router-dom";
import { configuredStore } from "store/index";
import Routes from "components/Routes";
import Navigation from "../__ui/Navigation";

export const store = configuredStore();

function App() {
  return (
    <Provider store={store}>
      <HashRouter>
        <Navigation/>
        <Routes />
      </HashRouter>
    </Provider>
  );
}

export default App;
