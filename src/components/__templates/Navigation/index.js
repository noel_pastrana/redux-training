import React from "react";
import { Link } from "react-router-dom";
import "./index.scss";

const Navigation = () => {

  return (
    <div className="navigation">
      <Link to="/" >Home</Link>
      <Link to="/contact" >Contact</Link>
      <Link to="/product" >Product</Link>
    </div>
  )
};

export default Navigation;