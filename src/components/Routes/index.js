import React from "react";
import { Switch, Route } from "react-router-dom";
import { ROUTES_CONFIG } from "./config";


const Routes = () => {
  return (
      <Switch>
        
      {ROUTES_CONFIG.map((route, i) => {
        return (
          <Route
            key={i}
            exact={!!route.exact}
            path={route.path}
            render={() => route.component}
          />
        );
      })}
      <Route component={() => <div>404</div>} />
    </Switch>

  );
};

export default Routes;
