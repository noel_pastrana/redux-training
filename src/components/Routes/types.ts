export interface RouteType {
  path: string;
  component: React.ReactNode;
  exact?: true;
  routes?: RouteType[];
}
