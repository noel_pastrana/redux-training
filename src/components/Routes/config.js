import React from "react";
import HomePage from "../__pages/HomePage";
import ProductPage from "../__pages/ProductPage";
import JoinUsPage from "../__pages/JoinUsPage";
import ContactUs from "../__pages/ContactUsPage";

// import HomePage from "components/__pages/HomePage";
// import ContactPage from "components/__pages/ContactPage";

export const ROUTES_CONFIG = [
  { path: "/", exact: true, component: <HomePage/>},
  { path: "/product", component: <ProductPage/> },
  { path: "/join-us", component: <JoinUsPage/> },
  { path: "/contact-us", component: <ContactUs/> }
];
