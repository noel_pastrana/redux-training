import React, { Component } from "react";
import "./index.scss";
import Switcher from "components/__ui/Switcher";

const options = ["Cosmetics", "Clothes", "Electronics", "Anything"];

class ContactPage extends Component {
  state = {
    activeOption: ""
  };

  onOptionClick = active => {
    this.setState({ activeOption: active });
  };

  render() {
    return (
      <div className="home">
        CONTACT
        <Switcher
          options={options}
          onOptionClick={this.onOptionClick}
          active={this.state.activeOption}
        />
      </div>
    );
  }
}

export default ContactPage;
