import React, { Fragment, Component } from 'react';
import "./index.scss";
import { fetchUsers, fetchUserById, selectUserId } from '../../../store/actionCreators/user';
import { connect } from "react-redux";
import UserList from '../../__ui/UserList';
import UserInfo from '../../__ui/UserInfo';

export class HomePage extends Component {

    constructor(props) {
        super(props);
    }
    

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(fetchUsers());
    }

    onClickHandleUser = (user) => {
        const {dispatch} = this.props;
        dispatch(selectUserId(user.id));
    }


    render() {

        const {users, user} = this.props;
        console.log(users)

        return (
            <Fragment>
                <div className="container-fluid">

                    <div className="row">
                        <div className="col-xs-12 col-md-6">
                            <UserList onClickHandleUser={this.onClickHandleUser} users={users}/>
                        </div>
                        <div className="col-xs-12 col-md-6">
                            Show User information here
                            {user}
                            <UserInfo user={user} />
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}


const mapStateToProps = (state) => ({
        users: state.users.users,
        user: state.userId
});
export default connect(mapStateToProps)(HomePage);