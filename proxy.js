const http = require('http');
const {Proxy} = require('@domoinc/ryuu-proxy');

const manifest = require('./public/manifest.json');
const domoProxy = new Proxy({manifest, appContextId: manifest.id})

const server = http.createServer((req, res) => {
  domoProxy
    .stream(req)
    .then(stream => stream.pipe(res))
    .catch((err) => {
      const status = typeof err !== 'undefined' ? (err.status || err.statusCode || 500) : 500;
      res.writeHead(status);
      res.end(JSON.stringify(err))
    });
  delete req.headers['x-forwarded-host']
});

server.listen(4000);
